

export const isNumber = (value) => {
    return /[0-9]+/.test(value);
};

export const operation = (numberOne, numberTwo, operation) => {
    const one = numberOne || "0";
    const two = numberTwo || "0";

    if (operation === "+") {
        return Number(one) + Number(two);
    }
    if (operation === "-") {
        return Number(one) - Number(two);
    }
    if (operation === "x") {
        return Number(one) * Number(two);
    }
    if (operation === "/") {
        if (two === "0") {
            alert("Divide by 0 error");
            return "0";
        } else {
            return Number(one) / Number(two);
        }
    }

    if (operation === "%") {
        if (two === "0") {
            alert("Divide by 0 error");
            return "0";
        } else {
            return Number(one) / Number(two);
        }
    }
    throw Error(`Unknown operation '${operation}'`);
};

