import React from "react";
import "./Button.css";

const Button = (props) => {

    const styles = [
        "Button",
        props.orange ? "orange" : "",
        props.wide ? "wide" : "",
    ];

    return (
        <div className={styles.join(" ").trim()}>
            <button onClick={() => props.clickHandler(props.name)}>{props.name}</button>
        </div>
    );
}

export default Button;