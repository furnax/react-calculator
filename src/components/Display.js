import React from 'react';
import './Display.css';

const Display = (props) => {
    return (
        <div className="Display">
            <div>{props.children}</div>
        </div>
    );
};

export default Display;