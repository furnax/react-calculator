import React from 'react';
import './Calculator.css';

import Display from "./Display";
import Keyboard from "./Keyboard";

import {isNumber, operation} from "../lib/calc";


class Calculator extends React.Component {

    state = {
        total: null,
        input: null,
        operation: null,
    };

    componentDidMount() {
        window.addEventListener("keydown", this.handleKeyDown.bind(this))
    }

    handleKeyDown = (event) => {
        const key = event.key;

        if (isNumber(key)) {
            this.handleKey(key)
        } else {
            switch (event.key) {
                case '+':
                case '-':
                case '/':
                case '=':
                case 'Enter':
                    this.handleKey(event.key);
                    break;
            }
        }
    };

    calculate(state, command) {
        if (command === "AC") {
            return {
                total: null,
                input: null,
                operation: null,
            };
        }

        if (isNumber(command)) {
            // if (command === "0" && state.input === "0") {
            //     return {};
            // }

            // If there is an operation, update input
            if (state.operation) {
                if (state.input) {
                    return {input: state.input + command};
                }
                return {input: command};
            }
            // If there is no operation, update input and clear the value
            if (state.input) {
                const input = state.input === "0" ? command : state.input + command;
                return {
                    input,
                    total: null,
                };
            }
            return {
                input: command,
                total: null,
            };
        }

        if (command === ".") {
            if (state.input) {
                // ignore a . if the input number already has one
                if (state.input.includes(".")) {
                    return {};
                }
                return {input: state.input + "."};
            }
            return {input: "0."};
        }

        if (command === "+/-") {
            if (state.input) {
                return {input: (-1 * parseFloat(state.input)).toString()};
            }
            if (state.total) {
                return {total: (-1 * parseFloat(state.total)).toString()};
            }
            return {};
        }

        if (command === "=" || command === "Enter") {
            if (state.input && state.operation) {
                return {
                    total: operation(state.total, state.input, state.operation),
                    input: null,
                    operation: null,
                };
            } else {
                // '=' with no operation, nothing to do
                return {};
            }
        }

        //+, -, *, /, %

        // User pressed an operation button and there is an existing operation
        if (state.operation) {
            return {
                total: operation(state.total, state.input, state.operation),
                input: null,
                operation: command,
            };
        }

        // no operation yet, but the user typed one

        // The user hasn't typed a number yet, just save the operation
        if (!state.input) {
            return {operation: command};
        }

        // save the operation and shift 'input' into 'total'
        return {
            total: state.input,
            input: null,
            operation: command,
        };
    }

    handleKey = (name) => {
        const currentState = {
            ...this.state
        };

        const newState = this.calculate(currentState, name);

        this.setState(newState)
    };

    render() {
        return (
            <div className="Calculator" style={{backgroundColor: "red"}}>
                <Display>{this.state.input || this.state.total || 0}</Display>
                <Keyboard clickHandler={(name) => this.handleKey(name)}/>
            </div>
        );
    }
}


export default Calculator;