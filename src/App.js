import React from 'react';
import './App.css';

import Calculator from "./components/Calculator";

function App() {
    return (
        <div className="App">
          <section className="calculator">
            <Calculator />
          </section>
        </div>
    );
}

export default App;
